import Exceptions.CreditoInvalidoException;
import Exceptions.InsuficienteDineroException;
import Exceptions.LimiteDineroException;
import Exceptions.MaxBarcosException;


/**
 * @author Elliot
 */
public abstract class Producto {

	private String referencia; // C�digo �nico del producto.
	private String modelo; // Modelo del producto.
	private String descripcion; // Breve descripci�n del producto.
	private double precio; // Precio del producto.
	private boolean comprado; // Indica si el producto se ha comprado (TRUE) o
								// no (FALSE).

	// Constructor de producto
	/**
	 * @param r
	 *            Referencia
	 * @param m
	 *            Modelo
	 * @param d
	 *            Corta descripci�n
	 * @param p
	 *            Precio
	 */
	public Producto(String r, String m, String d, double p) {
		setReferencia(r);
		setModelo(m);
		setDescripcion(d);
		setPrecio(p);
		setComprado(false);
	}

	// M�todo para comprar
	public void comprar(Cliente c) throws InsuficienteDineroException, LimiteDineroException, CreditoInvalidoException {
		if (c.getDinero() >= this.getPrecio()) {
			c.setDinero(c.getDinero() - this.getPrecio());
			this.setComprado(true);
				c.addCompra(this.getReferencia(), this);
		} else {
			System.err.println("ERROR: El cliente no tiene suficiente dinero.");
		}
	}

	// M�todo para comprar
	public void vender(Cliente c) {
		if (c.getCompras().containsKey(this.referencia) == true) {
			c.setDinero(c.getDinero() + this.getPrecio());
			this.setComprado(false);
			c.removeCompra(this);
		} else {
			System.err.println("ERROR: El cliente no dispone de ese prodcuto.");
		}
	}

	// Setters y Getters de los atributos de Producto
	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public boolean isComprado() {
		return comprado;
	}

	public void setComprado(boolean comprado) {
		this.comprado = comprado;
	}

}
