package Exceptions;

public class InsuficienteDineroException extends Throwable {

	private String message;
	
	public InsuficienteDineroException(String cause) {
		message=cause;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
