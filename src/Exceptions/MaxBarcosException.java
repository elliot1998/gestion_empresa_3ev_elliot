package Exceptions;

public class MaxBarcosException extends Exception {
	
	private String message;
	
	public MaxBarcosException(String cause) {
		message=cause;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
