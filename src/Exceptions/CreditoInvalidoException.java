package Exceptions;

public class CreditoInvalidoException extends Throwable {

	private String message;
	
	public CreditoInvalidoException(String cause) {
		message=cause;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
