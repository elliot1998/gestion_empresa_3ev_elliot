package Exceptions;

public class LimiteDineroException extends Throwable {

	private String message;
	
	public LimiteDineroException(String cause) {
		message=cause;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
