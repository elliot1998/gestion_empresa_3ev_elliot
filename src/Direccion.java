/**
 * @author Elliot
 */
public class Direccion {

	private String calle;		// Nombre de la calle.
	private String numero;		// N�mero de la vivienda.
	private String poblacion;	// Nombre de la poblaci�n.
	private String postal;		// C�digo postal.
	private String pais;		// Nombre del pa�s.
	

// Constructor de Direcci�n
	/**
	 * @param c Calle
	 * @param n N�mero
	 * @param pob Poblaci�n
	 * @param post C�digo Postal
	 * @param nacion Pa�s
	 */
	public Direccion (String c, String n, String pob, String post, String nacion) {
		calle = c;
		numero = n;
		poblacion = pob;
		postal = post;
		pais = nacion;
	}
	
	
// Getters y Setters de los atributos de Direccion.
	public String getCalle() {
		return calle;
	}
	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getPoblacion() {
		return poblacion;
	}
	public void setPoblacion(String poblacion) {
		this.poblacion = poblacion;
	}

	public String getPostal() {
		return postal;
	}
	public void setPostal(String postal) {
		this.postal = postal;
	}

	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}


}
