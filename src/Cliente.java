import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import Exceptions.CreditoInvalidoException;
import Exceptions.InsuficienteDineroException;
import Exceptions.LimiteDineroException;
import Exceptions.MaxBarcosException;

/**
 * @author Elliot
 */
public abstract class Cliente {

	private String nombre; // Nombre del cliente.
	private String apellidos; // Apellidos del cliente.
	private String dni; // DNI del cliente.
	private int edad; // Edad del cliente.
	private double dinero; // Presupuesto del cliente.
	private Direccion direccion; // Direcci�n del cliente.
	private HashMap<String, Producto> compras; // Compras realizadas por el
												// cliente.
	private int barcos; // N�mero de barcos que tiene el cliente.

	// Constructor de Cliente
	/**
	 * @param n
	 *            Nombre
	 * @param a
	 *            Apellidos
	 * @param dni
	 *            DNI
	 * @param e
	 *            Edad
	 * @param d
	 *            Dinero disponible
	 * @param dir
	 *            Direcci�n (Calle, N�mero, Poblaci�n, C�digo Postal, Pa�s)
	 */
	public Cliente(String n, String a, String dni, int e, double d, Direccion dir) {
		setNombre(n);
		setApellidos(a);
		setDni(dni);
		setEdad(e);
		setDinero(d);
		setDireccion(dir);
		compras = new HashMap<String, Producto>();
		barcos = 0;
	}

	// Getters y Setters de los atributos de Cliente.
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public double getDinero() {
		return dinero;
	}

	public void setDinero(double dinero) {
		this.dinero = dinero;
	}

	public Direccion getDireccion() {
		return direccion;
	}

	public void setDireccion(Direccion direccion) {
		this.direccion = direccion;
	}

	public HashMap<String, Producto> getCompras() {
		return compras;
	}

	public void setCompras(HashMap<String, Producto> compras) {
		this.compras = compras;
	}

	public int getBarcos() {
		return barcos;
	}

	public void setBarcos() throws MaxBarcosException {
		if (this.barcos >= 2) {
			this.barcos = 2;
			throw new MaxBarcosException("No se permiten comprar m�s de dos barcos");

		} else {
			this.barcos++;
		}
	}

	// M�todos para a�adir y quitar compras
	public abstract void addCompra(String ref, Producto compra)
			throws InsuficienteDineroException, LimiteDineroException, CreditoInvalidoException;

	public abstract void removeCompra(Producto compra);

}
