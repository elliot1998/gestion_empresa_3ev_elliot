
public class Piscina extends Producto {

	private float x, y, z; // Ejes de altura, anchura y profundidad
	private float volumen; // Cantidad de litros que cabe en el inteerior de la
							// piscina

	// Constructor de la Piscina
	/**
	 * @param r
	 *            Referencia
	 * @param m
	 *            Modelo
	 * @param d
	 *            Corta descripción
	 * @param p
	 *            Precio
	 * @param x
	 *            Altura
	 * @param y
	 *            Anchura
	 * @param z
	 *            Profundidad
	 */
	public Piscina(String r, String m, String d, double p, float x, float y, float z) {
		super(r, m, d, p);
		setX(x);
		setY(y);
		setZ(z);
		setVolumen();
		setPrecio(getPrecio() * (1 + (volumen / 200)));
		setReferencia("P" + r);
	}

	// Getter y Setter de los atributos de Piscina
	/**
	 * @return Devuelve la altura de la Piscina en metros
	 */
	public float getX() {
		return x;
	}

	/**
	 * @param x
	 *            Establece la altura de la Piscina en metros
	 */
	public void setX(float x) {
		if (x < 0) {
			this.x = 0;
		} else {
			this.x = x;
		}
	}

	/**
	 * @return Devuelve la anchura de la Piscina en metros
	 */
	public float getY() {
		return y;
	}

	/**
	 * @param x
	 *            Establece la anchura de la Piscina en metros
	 */
	public void setY(float y) {
		if (y < 0) {
			this.y = 0;
		} else {
			this.y = y;
		}
	}

	/**
	 * @return Devuelve la profundidad de la Piscina en metros
	 */
	public float getZ() {
		return z;
	}

	/**
	 * @param x
	 *            Establece la profundidad de la Piscina en metros
	 */
	public void setZ(float z) {
		if (z < 0 || z > 5) {
			this.z = 0;
		} else {
			this.z = z;
		}
	}

	public float getVolumen() {
		return volumen;
	}

	public void setVolumen() {
		this.volumen = this.z * this.x * this.y;
	}

}
