import java.util.HashMap;

import Exceptions.InsuficienteDineroException;
import Exceptions.LimiteDineroException;
import Exceptions.MaxBarcosException;

public class ClienteNormal extends Cliente {
	
	private double dinero_gastado;
	
	// Constructor de ClienteNormal
		/**
		 * @param n Nombre
		 * @param a Apellidos
		 * @param dni DNI
		 * @param e Edad
		 * @param d Dinero disponible
		 * @param dir Direcci�n (Calle, N�mero, Poblaci�n, C�digo Postal, Pa�s)
		 */
		public ClienteNormal(String n, String a, String dni, int e, double d, Direccion dir) {
			super(n, a, dni, e, d, dir);
			setDinero_gastado(0.0);
		}


// Getter y Setter del dinero gastado
		public double getDinero_gastado() {
			return dinero_gastado;
		}
		public void setDinero_gastado(double dinero_gastado) {
			this.dinero_gastado = dinero_gastado;
		}
		
// Setter de Dinero_gastado al cual se le pasa un producto en ves de un double
		public void setDinero_gastado(Producto producto) {
			this.dinero_gastado = dinero_gastado+producto.getPrecio();
		}
// M�todo para restar dinero gastado cuando se vende un producto
		public void removeDinero_gastado(Producto producto) {
			this.dinero_gastado = dinero_gastado-producto.getPrecio();
		}


// M�todos abstractos de Cliente implementados
		@Override
		public void addCompra(String ref, Producto compra) throws InsuficienteDineroException, LimiteDineroException {
			if (dinero_gastado+compra.getPrecio()<1000) {		// Los clientes normales no tienen permitido gastar 1000+
				
				if (getDinero()>=compra.getPrecio()) {		// Entra si el cliente tiene dinero para pagar el producto
					
					if (compra instanceof Barco) {
						try {
							setBarcos();
						} catch (MaxBarcosException e) {
							e.printStackTrace();
						}
						
					}
						
					getCompras().put(ref, compra);
					setDinero_gastado(compra);
					setDinero(getDinero()-compra.getPrecio());
					
					
				} else {
					throw new InsuficienteDineroException("El cliente no tiene suficiente dinero para comprar este producto");
				}
				
			} else {
				throw new LimiteDineroException("Este cliente no puede gastar m�s de 1000�");
			}
			
		}
		
		@Override
		public void removeCompra(Producto compra)  {
			getCompras().remove(compra);
			removeDinero_gastado(compra);
			setDinero(getDinero()+compra.getPrecio());
		}
	
	
}
