import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;

import Exceptions.CreditoInvalidoException;
import Exceptions.InsuficienteDineroException;
import Exceptions.LimiteDineroException;

/**
 * @author Elliot
 */
public class CompraVenta {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);	// Scanner
		int opcion = 11;						// Opci�n elegida por el usuario
		Producto producto;						// Producto a instanciar
		int numPremium=0;						// N�mero de clientes premium
		
		// Lista de clientes
		HashMap<String, Cliente> clientes = new HashMap<String, Cliente>();
		
		// Lista de productos
		HashMap<String, Producto> productos = new HashMap<String, Producto>();
		
		while (opcion != 0) {
			
			// Men� de opciones
			System.out.println("GESTI�N DE EMPRESA\n"
					+ "***********************************\n"
					+ "1.  Introducir Cliente.\n"
					+ "2.  Introducir Producto.\n"
					+ "3.  Comprar Producto.\n"
					+ "4.  Vender Producto.\n"
					+ "5.  Eliminar Cliente.\n"
					+ "6.  Eliminar Producto.\n"
					+ "7.  Listar Clientes.\n"
					+ "8.  Listar Productos.\n"
					+ "9.  Cargar Clientes.\n"
					+ "10. Cargar Productos.\n"
					+ "11. Guardar Cliente.\n"
					+ "12. Guardar Producto\n"
					+ "0.  Salir\n");
		
			opcion = sc.nextInt();
			
			switch (opcion) {
				// EXIT
				case 0:
					System.out.println("Hasta luego.");
				break;
			
				// INSERTAR CLIENTE
				case 1: 
					// Tipo de cliente
					int tipoCliente;
					
					if (numPremium<5) {
						System.out.println("Normal [1] --- Premium [2]");
						System.out.print("Elige tipo de cliente: ");
						tipoCliente = sc.nextInt();
					} else {
						tipoCliente = 1;
					}
					
					
					// Nombre del cliente
					System.out.print("Dime el nombre del cliente: ");
					String nombre;
					nombre = sc.nextLine();
					nombre = sc.nextLine();
					
					// Apellidos
					System.out.print("Dime los apellidos del cliente: ");
					String apellidos;
					apellidos = sc.nextLine();
					
					// DNI
					System.out.print("Dime el DNI del cliente: ");
					String dni;
					dni = sc.nextLine();
					
					// Edad
					System.out.print("Dime la edad del cliente: ");
					int edad;
					edad = sc.nextInt();
					
					// Dinero
					System.out.print("Dime de cuanto dinero dispone el cliente: ");
					double dinero;
					dinero = sc.nextDouble();
					
					// Direcci�n
					System.out.print("Dime la calle donde vive el cliente: ");
					String calle;
					calle = sc.nextLine();
					calle = sc.nextLine();
					
					System.out.print("Dime el n�mero de la vivienda del cliente: ");
					String numero;
					numero = sc.nextLine();
					
					System.out.print("Dime la poblaci�n donde vive cliente: ");
					String poblacion;
					poblacion = sc.nextLine();
					
					System.out.print("Dime el codigo postal de donde vive cliente: ");
					String postal;
					postal = sc.nextLine();
					
					System.out.print("Dime el pa�s donde vive cliente: ");
					String pais = sc.nextLine();
					
					// Instanciaci�n de la direcci�n
					Direccion dir = new Direccion(calle, numero, poblacion, postal, pais);
					
					// Instanciaci�n del cliente
					Cliente cliente;
					
					if (tipoCliente == 1) {
						cliente = new ClienteNormal(nombre, apellidos, dni, edad, dinero, dir);
						
						// Se guarda el cliente en la lista
						clientes.put(dni, cliente);
						
					} else if (tipoCliente == 2) {
						cliente = new ClientePremium(nombre, apellidos, dni, edad, dinero, dir);
						numPremium++;
						
						// Se guarda el cliente en la lista
						clientes.put(dni, cliente);
					}
					
					// Se pone a null las variables
					cliente = null;
					dir = null;
				break;
				
				
				// INSERTAR PRODUCTO
				case 2:
					// Tipo de producto
					System.out.println("Barco [1] --- Piscina [2] --- Tabla de surf [3]");
					System.out.print("Elige producto: ");
					int tipoProducto;
					tipoProducto = sc.nextInt();
					
					// Referencia del prodcuto
					System.out.print("Dime la referencia del producto: ");
					String referencia;
					referencia = sc.nextLine();
					referencia = sc.nextLine();
					
					// Modelo
					System.out.print("Dime el modelo del producto: ");
					String modelo;
					modelo = sc.nextLine();
					
					// Descripci�n
					System.out.print("Dime la descripci�n del producto: ");
					String desc;
					desc = sc.nextLine();
					
					// Precio
					System.out.print("Dime el precio del producto: ");
					Double precio;
					precio = sc.nextDouble();
					
					// Preguntas si el producto es un barco
					if (tipoProducto==1) {
						// Eslora
						System.out.print("Dime la eslora del barco: ");
						float eslora;
						eslora = sc.nextFloat();
						
						// Nudos
						System.out.print("Dime los nudos del barco: ");
						int nudos;
						nudos = sc.nextInt();
						
						// Instanciaci�n de producto
						producto = new Barco(referencia, modelo, desc, precio, eslora, nudos);
						
						// Se guarda el producto en la lista
						productos.put(referencia, producto);
					}
					
					// Preguntas si el producto es una piscina
					if (tipoProducto==2) {
						// Altura
						System.out.print("Dime la altura de la piscina: ");
						float x;
						x = sc.nextFloat();
						
						// Anchura
						System.out.print("Dime la anchura de la piscina: ");
						float y;
						y = sc.nextFloat();
						
						// Profundidad
						System.out.print("Dime la profundidad de la piscina: ");
						float z;
						z = sc.nextFloat();
						
						// Instanciaci�n de producto
						producto = new Piscina(referencia, modelo, desc, precio, x, y, z);
						
						// Se guarda el producto en la lista
						productos.put(referencia, producto);
					}
					
					// Preguntas si el producto es una tabla de surf
					if (tipoProducto==3) {
						// Material
						System.out.print("Dime el material de la tabla: ");
						String material;
						material = sc.nextLine();
						material = sc.nextLine();
						
						// Peso m�ximo
						System.out.print("Dime el peso m�ximo que soporta la tabla: ");
						float peso;
						peso = sc.nextFloat();
						
						// Instanciaci�n de producto
						producto = new TablaSurf(referencia, modelo, desc, precio, peso, material);
						
						// Se guarda el producto en la lista
						productos.put(referencia, producto);
					}
					
					// Se ponen la variable a null
					producto = null;
				break;
				
				
				// COMPRAR PRODUCTO
				case 3:
					// Referencia del producto
					System.out.print("Inserta la referencia del producto: ");
					String ref;
					ref = sc.nextLine();
					ref = sc.nextLine();
					
					// DNI del cliente
					System.out.print("Inserta el DNI del cliente: ");
					String d;
					d = sc.nextLine();
					
					// Se llama al metodo de compra
				try {
					productos.get(ref).comprar(clientes.get(d));
				} catch (InsuficienteDineroException e1) {
					e1.printStackTrace();
				} catch (LimiteDineroException e1) {
					e1.printStackTrace();
				} catch (CreditoInvalidoException e1) {
					e1.printStackTrace();
				}
				break;
				
				
				// VENDER PRODUCTO
				case 4:
					// Referencia del producto
					System.out.print("Inserta la referencia del producto: ");
					String refVenta;
					refVenta = sc.nextLine();
					refVenta = sc.nextLine();
					
					// DNI del cliente
					System.out.print("Inserta el DNI del cliente: ");
					String dVenta;
					dVenta = sc.nextLine();
					
					// Se llama al metodo de venta
					productos.get(refVenta).vender(clientes.get(dVenta));
				break;
				
				
				// ELIMINAR CLIENTE
				case 5:
					// DNI del cliente
					System.out.print("Inserta el DNI del cliente: ");
					String dEliminar;
					dEliminar = sc.nextLine();
					dEliminar = sc.nextLine();
					
					if (clientes.get(dEliminar) instanceof ClientePremium) {
						numPremium--;
					}
					
					// Se elimina el cliente
					clientes.remove(dEliminar);
				break;
				
				
				// ELIMINAR PRODUCTO
				case 6:
					// Referencia del producto
					System.out.print("Inserta la referencia del producto: ");
					String refEliminar;
					refEliminar = sc.nextLine();
					refEliminar = sc.nextLine();
					
					// Se elimina el cliente
					productos.remove(refEliminar);
				break;
				
				
				// LISTAR CLIENTES
				case 7:
					// Se imprime el map con el Iterator
					Iterator i = clientes.values().iterator();
					while(i.hasNext()){
					  Cliente c = (Cliente) i.next();
					  System.out.println("Clave: " + c.getDni() + " -> Valor:\nTipo: "+c.getClass().getName()+"\n\tNombre: " + c.getNombre() + "\n\tApellidos: "
					  		+ c.getApellidos() + "\n\tEdad: " + c.getEdad() + "\n\tDinero: " + c.getDinero()
					  		+ "\n\tDirecci�n: " + c.getDireccion().getCalle() + ", " + c.getDireccion().getNumero()
					  		+ ", " + c.getDireccion().getPoblacion() + ", " + c.getDireccion().getPais()
					  		+ "\n\tC�digo Postal: " + c.getDireccion().getPostal());
					}
				break;
				
				
				// LISTAR PRODUCTOS
				case 8:
					// Se imprime el map con el Iterator
					Iterator it = productos.keySet().iterator();
					while(it.hasNext()){
					  Producto p = (Producto) it.next();
					  System.out.println("Clave: " + p.getReferencia() + " -> Valor:\n\tModelo: " + p.getModelo()
							  + "\n\tDescripci�n: " + p.getDescripcion() + "\n\tPrecio: " + p.getPrecio());
					}
				break;
				
				
				// CARGAR CLIENTES
				case 9:
					// Se carga la lista de clientes de un fichero
				FileInputStream fis;
				DataInputStream dis;
				
				System.out.print("Introduce la ruta del fichero: ");
				String ruta_clientes = sc.nextLine();
				
				try {
					fis = new FileInputStream(ruta_clientes);
					dis = new DataInputStream(fis);
					
					while (fis.read()!=-1) {
						System.out.println(fis.read());
					}
					fis.close();
					dis.close();
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				break;
				
				
				// CARGAR PRODUCTOS
				case 10:
					// Se cargan productos desde un fichero
					FileInputStream fis_p;
					DataInputStream dis_p;
					
					System.out.print("Introduce la ruta del fichero: ");
					String ruta_productos = sc.nextLine();
					
					try {
						fis_p = new FileInputStream(ruta_productos);
						dis_p = new DataInputStream(fis_p);
						
						while (fis_p.read()!=-1) {
							System.out.println(fis_p.read());
						}
						
						fis_p.close();
						dis_p.close();
						
					} catch (FileNotFoundException e1) {
						e1.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
				break;
				
				
				// GUARDAR CLIENTE
				case 11:
					// Guardamos un cliente en un fichero
					FileWriter fw= null;
					
					System.out.print("Dime el nombre del fichero donde quieres guardar el cliente: ");
					String fichero_cg = sc.nextLine();
					
					System.out.print("Dime el dni del cliente a guardar: ");
					String cliente_g = sc.nextLine();
					
				try {
					fw=new FileWriter(fichero_cg);
					fw.write(clientes.get(cliente_g).toString());
				} catch (IOException e1) {
					e1.printStackTrace();
				} finally {
					try {
						fw.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				break;
				
				
				// GUARDAR PRODUCTO
				case 12:
					// Guardamos un cliente en un fichero
					FileWriter fw_p= null;
					
					System.out.print("Dime el nombre del fichero donde quieres guardar el producto: ");
					String fichero_pg = sc.nextLine();
					
					System.out.print("Dime la referencia del producto a guardar: ");
					String producto_g = sc.nextLine();
					
				try {
					fw_p=new FileWriter(fichero_pg);
					fw_p.write(productos.get(producto_g).toString());
				} catch (IOException e1) {
					e1.printStackTrace();
				} finally {
					try {
						fw_p.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				break;
				
				
				// DEFAULT: Mensaje de error
				default:
					System.err.println("Introduce una opci�n v�lida, por favor.");
					
					// Esperamos 3 segundos antes de vovler a mostrar el men�
					try {
						Thread.sleep(3000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
			}
			System.out.println("\n\n\n\n\n");
		}
	}

}
