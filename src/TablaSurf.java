import java.util.Scanner;

public class TablaSurf extends Producto {

	private float peso_max;
	private String material;

	// Constructor de la TablaSurf
	/**
	 * @param r
	 *            Referencia
	 * @param m
	 *            Modelo
	 * @param d
	 *            Corta descripci�n
	 * @param p
	 *            Precio
	 */
	public TablaSurf(String r, String m, String d, double p, float pm, String mat) {
		super(r, m, d, p);
		setPeso_max(pm);
		setMaterial(mat);
		if (getMaterial().equalsIgnoreCase("fibra")) {
			setPrecio(getPrecio()*1.2);
		}
		setReferencia("TS" + r);
	}

	// Setters y Getters de los atributos de TablaSurf
	public float getPeso_max() {
		return peso_max;
	}

	public void setPeso_max(float peso_max) {
		this.peso_max = peso_max;
	}

	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		Scanner sc = new Scanner(System.in);
		if (material.equalsIgnoreCase("madera") || material.equalsIgnoreCase("fibra")
				|| material.equalsIgnoreCase("pvc")) {
			this.material = material;
		} else {
			while (this.material != "madera" && this.material !="fibra" && this.material != "pvc") {
				System.out.println("Madera [1] --- Fibra [2] --- Pvc [3]");
				System.out.print("Por favor introduzca una opci�n v�lida: ");
				int select = sc.nextInt();
				if (select == 1) {
					this.material = "madera";
				} else if (select == 2) {
					this.material = "fibra";
				} else if (select == 3) {
					this.material = "pvc";
				}
			}
		}
		
	}

}
