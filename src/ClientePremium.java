import Exceptions.CreditoInvalidoException;
import Exceptions.InsuficienteDineroException;
import Exceptions.MaxBarcosException;

public class ClientePremium extends Cliente {

	private double credito;
	
// Constructor de ClientePremium
	/**
	 * @param n Nombre
	 * @param a Apellidos
	 * @param dni DNI
	 * @param e Edad
	 * @param d Dinero disponible
	 * @param dir Direcci�n (Calle, N�mero, Poblaci�n, C�digo Postal, Pa�s)
	 */
	public ClientePremium(String n, String a, String dni, int e, double d, Direccion dir) {
		super(n, a, dni, e, d, dir);
		try {
			setCredito(0);
		} catch (CreditoInvalidoException e1) {
			e1.printStackTrace();
		}
	}
	
// Getters y Setters
	public double getCredito() {
		return credito;
	}
	public void setCredito(double credito) throws CreditoInvalidoException {
		if (credito>0 && credito<=3000) {
			this.credito = credito;
		} else {
			throw new CreditoInvalidoException("El cr�dito no puede ser negativo ni mayor que 3000�");
		}
		
	}


// M�todos abstractos de Cliente implementados
	@Override
	public void addCompra(String ref, Producto compra) throws InsuficienteDineroException {
		if (getDinero()+getCredito()>=compra.getPrecio()) {
			if (compra instanceof Barco) {
				
				if (getBarcos() < 2) {
					try {
						setBarcos();
					} catch (MaxBarcosException e) {
						e.printStackTrace();
					}
					getCompras().put(ref, compra);
					
					if (getDinero()>=compra.getPrecio()) {
						setDinero(getDinero()-compra.getPrecio());
					} else {
						try {
							setCredito(getCredito()-(compra.getPrecio()-getDinero()));
						} catch (CreditoInvalidoException e) {
							e.printStackTrace();
						}
						setDinero(0);
						System.out.println("Se ha agotado su dinero, el resto del producto ha sido pagado con su cr�dito.");
					}
					
					if (compra.getPrecio()>500) {
						try {
							setCredito(getCredito()+10);
						} catch (CreditoInvalidoException e) {
							e.printStackTrace();
						}
					}
				}
				
			} else {
				
				getCompras().put(ref, compra);
				
				if (getDinero()>=compra.getPrecio()) {
					setDinero(getDinero()-compra.getPrecio());
				} else {
					try {
						setCredito(getCredito()-(compra.getPrecio()-getDinero()));
					} catch (CreditoInvalidoException e) {
						e.printStackTrace();
					}
					setDinero(0);
					System.out.println("Se ha agotado su dinero, el resto del producto ha sido pagado con su cr�dito.");
				}
				
				if (compra.getPrecio()>500) {
					try {
						setCredito(getCredito()+10);
					} catch (CreditoInvalidoException e) {
						e.printStackTrace();
					}
				}
			}
		} else {
			throw new InsuficienteDineroException("Este cliente no tiene suficiente dinero, ni creditos para comprr este producto");
		}
	}

	@Override
	public void removeCompra(Producto compra) {
		getCompras().remove(compra);
		setDinero(getDinero()+compra.getPrecio());
	}

}
