
public class Barco extends Producto {

	private float eslora; // Largo del barco
	private int nudos; // Velocidad m�xima del barco

	// Constructor del barco
	/**
	 * @param r
	 *            Referencia
	 * @param m
	 *            Modelo
	 * @param d
	 *            Corta descripci�n
	 * @param p
	 *            Precio
	 * @param e
	 *            Eslora (largo del barco)
	 * @param n
	 *            Nudos (velocidad m�xima)
	 */
	public Barco(String r, String m, String d, double p, float e, int n) {
		super(r, m, d, p);
		setEslora(e);
		setNudos(n);
		setReferencia("B" + r);
	}

	// Setters y Getters de los atributos de Barco
	public float getEslora() {
		return eslora;
	}

	public void setEslora(float eslora) {
		// La eslora no puede ser negativo ni medir m�s de 100
		if (eslora < 0 || eslora > 100) {
			this.eslora = 0;
		} else {
			this.eslora = eslora;

			// Si la eslora mide m�s de 20, se le a�ade un impuesto al precio
			if (this.eslora > 20) {
				setPrecio(getPrecio() * (1 + (this.eslora / 100)));
			}
		}
	}

	public int getNudos() {
		return nudos;
	}

	public void setNudos(int nudos) {
		this.nudos = nudos;
	}

}
